/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.User;
import persistence.UserDAO;

/**
 *
 * @author usuario
 */
public class userEdit extends HttpServlet {

    UserDAO userDao = new UserDAO();
    private static final Logger LOG = Logger.getLogger(userNew.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        response.setContentType("text/html;charset=UTF-8");

        User user = null; //variable para leer el usuario
        //tomamos los argumentos y buscamos el id
        List<String> args = (List<String>) request.getAttribute("args");
        int id;
        String nextJSP = "/WEB-INF/view/user/index.jsp";
        if (args.isEmpty()) {
            LOG.log(Level.INFO, "vacio");
            id = 0;
        } else {
            id = Integer.parseInt(args.remove(0));
            synchronized (userDao) {
                userDao.connect();
                user = userDao.get(id);
                userDao.disconnect();
            }
            request.setAttribute("user", user);
            nextJSP = "/WEB-INF/view/user/edit.jsp";
        }


        //reenviar request a la vista (JSP)
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
        //Si reenvio la petición a un servlet sería:
        //RequestDispatcher dispatcher = getServletContext().getNamedDispatcher("nombreServlet");
        //ahora reenviamos:
        dispatcher.forward(request, response);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
