/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.User;
import persistence.UserDAO;

/**
 *
 * @author alumno
 */
public class UserInsert extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private final UserDAO userDao = new UserDAO();
    private static final Logger LOG = Logger.getLogger(UserInsert.class.getName());

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        User user = null;
        int count = 0;
        if (request.getMethod().equalsIgnoreCase("post")) {
            user = new User(0,
                    request.getParameter("name"),
                    request.getParameter("surname"),
                    request.getParameter("login"),
                    "");
            LOG.info("Recogidos datos del formulario");
            synchronized(userDao){
                userDao.connect();
                count = userDao.insert(user);
                userDao.disconnect();
            }
        } else {
            user = new User();
            user.setName("anonimo");
            LOG.info("GET. Sin datos del formulario");
        }

        //añadir información al request para pasarla a la vista
        request.setAttribute("count", count);
        request.setAttribute("user", user);

        //reenviar request a la vista (JSP)
        String nextJSP = "/WEB-INF/view/user/insert.jsp";
//        nextJSP = "/WEB-INF/view/user/vacio.jsp";
        
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
        
//        request.getRequestDispatcher(nextJSP)
        //Si reenvio la petición a un servlet sería:
        //RequestDispatcher dispatcher = getServletContext().getNamedDispatcher("nombreServlet");
        //ahora reenviamos:
        dispatcher.forward(request, response);            
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
