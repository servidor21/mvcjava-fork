package persistence;

/*
 * Clase que se ocupa de la persistencia de USER
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import java.util.logging.Level;
import java.util.logging.Logger;
import model.User;

/**
 *
 * @author Rafa
 */
public class UserDAO {

    private final Object lockOfTheConexion = new Object();
    private Connection connection = null;
    private PreparedStatement stmt = null;
    private static final String CLASS_DRIVER = "com.mysql.jdbc.Driver";
//    private static final String URL ="jdbc:mysql://10.2.25.100/mvc";
    private static final String URL = "jdbc:mysql://10.0.2.15/mvc";
    private static final Logger LOG = Logger.getLogger(UserDAO.class.getName());
    private ResultSet rs;
    

    //metodo connect
    public void connect() {
        try {
            Class.forName(UserDAO.CLASS_DRIVER);
            connection = DriverManager.getConnection(UserDAO.URL, "usuario", "usuario");
//            stmt = connection.createStatement();            
            if (connection != null) {
                LOG.info("Conexión establecida!");
            } else {
                LOG.severe("Fallo de conexión!");
            }
        } catch (ClassNotFoundException ex) {
            LOG.log(Level.SEVERE,
                    "No se pudo cargar el driver de la base de datos", ex);
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE,
                    "No se pudo obtener la conexión a la base de datos", ex);
        }
    }
//metodo disconnect

    public void disconnect() {
        try {
            connection.close();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
    }
    //un metodo por cada operación CRUD, al menos

    //insert
    /**
     *
     * @param user
     * @return int
     * @throws SQLException
     */
    public int insert(User user) {
        try {
            stmt = connection.prepareStatement("insert into user values(DEFAULT,? ,? ,? , DEFAULT)");
            stmt.setString(1, user.getName());
            stmt.setString(2, user.getSurname());
            stmt.setString(3, user.getLogin());

            return stmt.executeUpdate();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
            return 0;
        }
    }
    public int update(User user) {
        try {
            stmt = connection.prepareStatement("update user set name=?, surname=?, login=? where id=?");
            stmt.setString(1, user.getName());
            stmt.setString(2, user.getSurname());
            stmt.setString(3, user.getLogin());
            stmt.setInt(4, user.getId());

            return stmt.executeUpdate();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    public ArrayList<User> getAll() {
        ArrayList<User> users = null;        
        try {
            stmt = connection.prepareStatement("select * from user");
            rs = stmt.executeQuery();
            users = new ArrayList();
            int i = 0;
            while (rs.next()) {
                i++;
                User user = new User();
                user.setId(rs.getInt("id"));
                user.setName(rs.getString("name"));
                user.setSurname(rs.getString("surname"));
                user.setLogin(rs.getString("login"));
                //other properties
                users.add(user);
                LOG.info("Registro fila: " + i);
            }
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return users;
    }
    
    public User get(int id)
    {
        User user = new User();
        LOG.info(user.toString());
        try {
            stmt = connection.prepareStatement("select * from user where id=?");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();            
            if (rs.next()) {
                user.setId(id);
                user.setName(rs.getString("name"));
                user.setSurname(rs.getString("surname"));
                user.setLogin(rs.getString("login"));
                user.setPassword("");
            } 
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
            return null;
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Falla por otra excepcion", ex);
            return null;
        }
        
        return user;
    }
    //delete
    public int delete(int id) {

        int count = 0;
        try {
            stmt = connection.prepareStatement("delete from user where id=?");
            stmt.setInt(1, id);
            count = stmt.executeUpdate();            
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
            count = 0;
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Falla por otra excepcion", ex);
            count = 0;
        }
        return count;
    }
    
    //
}
