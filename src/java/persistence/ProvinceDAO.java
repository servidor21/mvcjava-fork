package persistence;

/*
 * Clase que se ocupa de la persistencia de PROVINCE
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import java.util.logging.Level;
import java.util.logging.Logger;
import model.Province;

/**
 *
 * @author Rafa
 */
public class ProvinceDAO {

    private static final int PAGE_SIZE = 10;
    private Connection connection = null;
    private PreparedStatement stmt = null;
    private static final String CLASS_DRIVER = "com.mysql.jdbc.Driver";
//    private static final String URL ="jdbc:mysql://10.2.25.100/mvc";
    private static final String URL = "jdbc:mysql://10.0.2.15/mvc";
    private static final Logger LOG = Logger.getLogger(ProvinceDAO.class.getName());
    private ResultSet rs;
    

    //metodo connect
    public void connect() {
        LOG.info("Listos para conectar");
        try {
            Class.forName(ProvinceDAO.CLASS_DRIVER);
            connection = DriverManager.getConnection(ProvinceDAO.URL, "usuario", "usuario");
//            stmt = connection.createStatement();            
            if (connection != null) {
                LOG.info("Conexión establecida!");
            } else {
                LOG.severe("Fallo de conexión!");
            }
        } catch (ClassNotFoundException ex) {
            LOG.log(Level.SEVERE,
                    "No se pudo cargar el driver de la base de datos", ex);
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE,
                    "No se pudo obtener la conexión a la base de datos", ex);
        }
    }
//metodo disconnect

    public void disconnect() {
        try {
            connection.close();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
    }
    //un metodo por cada operación CRUD, al menos

    public ArrayList<Province> getAll(int page) {
        ArrayList<Province> provinces = null;        
        try {
            LOG.info("antes de preparar");
            stmt = connection.prepareStatement("select * from provincia limit " + PAGE_SIZE + " offset " + (page - 1) * PAGE_SIZE);
            LOG.info("2");
            rs = stmt.executeQuery();
            LOG.info("3");
            provinces = new ArrayList();
            int i = 0;
            while (rs.next()) {
                i++;
                Province province = new Province();
                province.setId(rs.getInt("id"));
                province.setProvincia(rs.getString("provincia"));
                //other properties
                provinces.add(province);
                LOG.info("Registro fila: " + i);
            }
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return provinces;
    }
    
    public Province get(int id)
    {
        Province province = new Province();
        LOG.info(province.toString());
        try {
            stmt = connection.prepareStatement("select * from provincia where id=?");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();            
            if (rs.next()) {
                province.setId(id);
                province.setProvincia(rs.getString("provincia"));
            } 
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
            return null;
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Falla por otra excepcion", ex);
            return null;
        }
        
        return province;
    }
    //delete
    public int delete(int id) {

        int count = 0;
        try {
            stmt = connection.prepareStatement("delete from province where id=?");
            stmt.setInt(1, id);
            count = stmt.executeUpdate();            
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
            count = 0;
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Falla por otra excepcion", ex);
            count = 0;
        }
        return count;
    }
    
    //
}
