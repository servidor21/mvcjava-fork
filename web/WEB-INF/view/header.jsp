<%@page import="java.util.Enumeration"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/default.css">
    <title>Framework MVC. Curso 2016/2017</title>
</head>
<body>
<div id="header">
<div id="title">Framework MVC (Java EE). Curso 2016/2017</div>
<div>
    <a href="<%= request.getContextPath() %>/index">Inicio</a>
    <a href="<%= request.getContextPath() %>/articulo">Artículo</a>
    <a href="<%= request.getContextPath() %>/user">Usuarios</a>
    <a href="<%= request.getContextPath() %>/province">Provincias</a>
    <a href="<%= request.getContextPath() %>/help">Ayuda</a>

    
<%
    Enumeration <String> names = session.getAttributeNames();
    while (names.hasMoreElements()) {
        if (names.nextElement().equals("province")){
            out.println(session.getAttribute("province") );
        }
    }
            
%>

</div>
</div>

    
