<%-- 
    Document   : edit
    Created on : 23-nov-2016, 20:54:12
    Author     : Rafa
--%>

<jsp:include page="/WEB-INF/view/header.jsp"/>   
<div id="content">

    <h1>Edici�n de usuario</h1>

    <jsp:useBean id="user" class="model.User" scope="request"/>  

    <form action="/mvcJava/user/update" method="post">
        <label >Id</label>
        <input type="text" name="id" readonly="readonly" 
               value="<%= user.getId()%>"><br>
        <label >Nombre</label>
        <input type="text" name="name"  
               value="<%= user.getName()%>"><br>
        <label >Apellido</label>
        <input type="text" name="surname" 
               value="<%= user.getSurname()%>"><br>
        <label >Login</label>
        <input type="text" name="login"  
               value="<%= user.getLogin()%>"><br>
        <input type="submit" value="Guardar">
    </form>

</div>
<jsp:include page="/WEB-INF/view/footer.jsp"/>