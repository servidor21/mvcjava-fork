<%-- 
    Document   : update
    Created on : 23-nov-2016, 21:18:37
    Author     : usuario
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Usuario guardado</h1>
        <jsp:useBean id="user" class="model.User" scope="request"/>  
        <jsp:useBean id="count" class="Integer" scope="request"/>  
        
        <% if (count.equals(0)) { %>
        <p>Registro no actualizado</p> 
        <% } else { %>
        <ul>
            <li><%= user.getLogin()%></li>
            <li><%= user.getSurname()%></li>
            <li><%= user.getLogin()%></li>
        </ul>
        <% } %>
    </body>
</html>
